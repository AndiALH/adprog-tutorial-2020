package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
        //ToDo: Complete me
        AttackBehavior attackBehavior;
        DefenseBehavior defenseBehavior;

        public String getAlias() {
                return "The Mystic Adventurer";
        }

        public MysticAdventurer() {
                super.setAttackBehavior(new AttackWithMagic());
                super.setDefenseBehavior(new DefendWithShield());
        }
}
