package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me
        public String getType() {
                return "Magic Attack type";
        }

        public String attack() {
                return "menyerang menggunakan Sihir";
        }
}
