package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        //ToDo: Complete me
        AttackBehavior attackBehavior;
        DefenseBehavior defenseBehavior;

        public String getAlias() {
                return "The Knight Adventurer";
        }

        public KnightAdventurer() {
                super.setAttackBehavior(new AttackWithSword());
                super.setDefenseBehavior(new DefendWithArmor());
        }
}
