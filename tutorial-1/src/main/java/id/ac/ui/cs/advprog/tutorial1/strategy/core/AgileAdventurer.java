package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
        //ToDo: Complete me
        AttackBehavior attackBehavior;
        DefenseBehavior defenseBehavior;

        @Override
        public String getAlias() {
                return "The Agile Adventurer";
        }

        public AgileAdventurer() {
                super.setAttackBehavior(new AttackWithGun());
                super.setDefenseBehavior(new DefendWithBarrier());
        }
}
